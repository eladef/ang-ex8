import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TodosService } from 'src/app/todos.service';


@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
@Input() data:any;
@Output() myButtonClicked= new EventEmitter<any>();

showTheButton = false; 
showEditField = false;
text;
tempText;
comment;
id;
key;
status: boolean;

showEdit()
{
  this.tempText = this.text;
  this.showTheButton = false;
  this.showEditField = true;
}

save()
{
  this.todosService.updateTodo(this.key,this.text, this.status);
  this.showEditField = false;
}

cancel()
{
  this.text = this.tempText;
  this.showEditField = false;
}

checkChange()
{
  this.todosService.updateStatus(this.key,this.text,this.status);
}



send(){

    this.myButtonClicked.emit(this.text);

      }

showButton(){
this.showTheButton=true;
}
hideButton(){
  this.showTheButton=false;
  }

delete(){
  this.todosService.delete(this.key);
}

constructor(private todosService:TodosService) { }

  ngOnInit() {
    this.text = this.data.text;
    this.comment = this.data.comment;
    this.id = this.data.id;

    //for delete todo
    this.key = this.data.$key;

  }

}
