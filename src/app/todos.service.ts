import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth'
import {Observable} from 'rxjs';
import {AngularFireDatabase} from '@angular/fire/database';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class TodosService {


   addTodo(text:string,status:boolean){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/todos').push({'text':text, 'status':false});
    })
  }

   delete(key){
     this.authService.user.subscribe(user=>{
      this.db.list('users/'+user.uid+'/todos').remove(key);
     })
   }

   updateTodo(key:string, text:string, status:boolean){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/todos').update(key,{'text':text,'status':status});
    })
  }

  updateStatus(key:string, text:string, status:boolean)
  {
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/todos').update(key,{'text':text, 'status':status});
    })
    
  }


  constructor(private db:AngularFireDatabase, private authService:AuthService) { }
}
