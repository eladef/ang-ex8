import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {AuthService} from '../auth.service';
import {TodosService} from 'src/app/todos.service';
import { Router } from '@angular/router';

@Component({
  selector: 'todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {
  

    //simple array that we traded with the firebase array (down below)
    //todos=[
    //  {"text":"text1", "id":1},
    //  {"text":"text2", "id":2},
    //  {"text":"text3", "id":3},
    //];

todos=[];
text:string;
todoTextFromTodo;
ifStatus:boolean; //בדיקה מה הסטטוס שבוחרים בפילטר
todoStatus:boolean; // סטטוס של טודו חדש שמכניסים
key;
taskStatus = [];

showText($event){
  this.todoTextFromTodo = ($event);
}


    addTodo(){
      this.todosService.addTodo(this.text, this.todoStatus);
      this.text = '';
      this.todoStatus = false;
      
    }

    showTodo()
    {
      this.authService.user.subscribe(user => {
        this.db.list('/users/' + user.uid + '/todos').snapshotChanges().subscribe(
          todos => {
            this.todos = [];
            todos.forEach(
              todo => {
                let y = todo.payload.toJSON();
                y["$key"] = todo.key;
                this.todos.push(y);
              }
            )
          }  
        )
      })
    }

    toFilter()
    {
      this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe(
        todos =>{
          this.todos = [];
          todos.forEach(
            todo => {
              let y = todo.payload.toJSON();
              y["$key"] = todo.key;          
                if (this.ifStatus == y['status']) {
                  this.todos.push(y);
                }
                else if (this.ifStatus != true && this.ifStatus != false)
                {
                  this.todos.push(y);
                }
                
            }
          )
        }
      )
      })
    }



  constructor(private router:Router, private db:AngularFireDatabase, private authService:AuthService , private todosService:TodosService ) {}

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe(
        todos =>{
          this.todos = [];
          this.taskStatus = [];
          todos.forEach(
            todo => {
              let y = todo.payload.toJSON();
              y["$key"] = todo.key;
              this.todos.push(y);
              let sta = y['status'];
              if (this.taskStatus.indexOf(sta)== -1)
               {
               this.taskStatus.push(sta);
              }
             
            }
          )
        }
      ) 
    })
  }

}
