import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';

@Component({
  selector: 'usertodos',
  templateUrl: './usertodos.component.html',
  styleUrls: ['./usertodos.component.css']
})
export class UsertodosComponent implements OnInit {


  todoTextFromTodo;

  showText($event){
    this.todoTextFromTodo = ($event);
  }
  todos=[];
  user='';


  constructor(private db:AngularFireDatabase) { }


  changeusers(){
      this.db.list('/users/' + this.user + '/todos').snapshotChanges().subscribe(
        todos => {
          this.todos=[];
          todos.forEach(
            todo =>{
            let y = todo.payload.toJSON();
            y['$key']= todo.key;
            this.todos.push(y);  
            }
          )
        }
      )
    }
  
  ngOnInit() {
    this.db.list('/users/' + this.user + '/todos').snapshotChanges().subscribe(
      todos => {
        this.todos=[];
        todos.forEach(
          todo =>{
          let y = todo.payload.toJSON();
          y['$key']= todo.key;
          this.todos.push(y);  
          }
        )
      }
    )
  }

}
